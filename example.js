class Machine {
    constructor() {
      this.turn = false;
    };
    turnOn ()
    {
        this.turn = true;
        console.log('Включить что-то!');
    }
            turnOf()
            {
                this.turn = false;
                console.log('Выключить что-то!');
            }
}
class HomeFppliance extends Machine {
    constructor() {
        super();
        this.plug = false;
    }

    plugIn() {
        this.plug = true;
        console.log('Включаяем в сеть!');
    }
    plugOf() {
        this.plug = false;
        console.log('Выключаем из сети!');
    }
}
class WashingMachine extends HomeFppliance {
    constructor() {
        super();
    }
    run() {
        if(!this.plug) {
            console.log('Включите В сеть!')
        } else {
            console.log('Запуск!');
        }

    }

}

let washingMachine = new WashingMachine();
washingMachine.plugIn();
washingMachine.run();

class LightSource extends HomeFppliance {
    constructor() {
        super();
        this.lightLevel = 0;
    }
    setLevel(level) {
        if (level>=1 && level<=100) {
            this.lightLevel = level;
        } else {
            console.log('Неверное значение')
        }
    }
}
let lightSource = new LightSource();
lightSource.setLevel(10);

class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }
    setPosition(x,y) {
        this.x = x;
        this.y = y;
    }

}
let autoVehicle = new AutoVehicle();
autoVehicle.setPosition(5,9);

class Car extends AutoVehicle {
    constructor(){
        super();
        this.speed = 10;
    }
    setSpeed(speed) {
        this.speed = speed;
    };
    run (x,y) {
        let interval = setInterval(() => {

            if (this.x < x) {
                this.setPosition(this.x+this.speed,this.y);
            }
            if (this.y < y) {
                this.setPosition(this.x,this.y+this.speed);
            }
            if( this.x > x || this.y > y) {
                this.x = x;
                this.y = y;
                clearInterval(interval);
            }
            console.log(this.x,this.y);
        }, 1000);

        console.log();
    }
}
let car = new Car();
car.setSpeed(35);
car.run(100,150);
let bosch = new WashingMachine();
bosch.plugIn();
bosch.turnOn();

let lightBulb = new LightSource();
lightBulb.plugIn();
lightBulb.setLevel(60);
lightBulb.turnOn();

let honda = new Car();
honda.setPosition(30, 40);
honda.turnOn();
honda.setSpeed(40);
honda.run(180, 240);